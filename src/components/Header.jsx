import React from "react";
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/header.css'

const Header = () => {
  return (
    <div className="header p-3 text-center">
      <h1>TRY GLASSES APP ONLINE</h1>
    </div>
  );
};

export default Header;
