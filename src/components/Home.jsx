import React from "react"
import Header from './Header'
import Item from './Item'
import '../styles/home.css'
import backgroundImage from '../image/background.png';


const Home = () => {
  return (
    <div>
      <div className="background" style={{backgroundImage:`url(${backgroundImage})`, backgroundSize:"cover", width:"100%", height: "100vh", backgroundPosition:"center"}}>
        {/* <img src="../glassesImage/background.jpg" alt="..."/> */}
        <Header/>
        <Item/>
      </div>
    </div>
  )
}

export default Home