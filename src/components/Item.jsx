import React, {useState} from "react";
import "bootstrap/dist/css/bootstrap.css";
import glassesList from "../data.json";
import "../styles/item.css";

const Item = () => {

  const [glassesImage, setGlassesImage] = useState('./glassesImage/v1.png')

  const handleChangeGlasses = (glasses) => {
    setGlassesImage(glasses.url)
  }
  return (
    <div className="container">
      <div className="model" style={{display:"flex", justifyContent:"space-between", alignItems:"center"}}>
        <div >
          <img className="glasses" style={{height: "40px", position:"relative", top:"-50px", zIndex:2, left:"185px", opacity:0.6}} src={glassesImage} alt="..." /> 
          <img src={"../glassesImage/model.jpg"} alt="..." />
        </div>
        <div>
          <img src="../glassesImage/model.jpg" alt="..." />
        </div>
      </div>  
      <div className="item">
        {glassesList.map((glasses) => {
          return (
            <div key={glasses.id}>
              {/* <div className="list"> */}
                <button onClick={() => handleChangeGlasses(glasses)} className="btn btn-outline-danger">
                  <img src={glasses.url} alt="" />
                </button>
              {/* </div> */}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Item;
